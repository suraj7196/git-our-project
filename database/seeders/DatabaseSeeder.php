<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\Theme;
use App\Models\SiteSetting;
use App\Models\Social;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Admin::insert([
            'name' => 'Waris Patel',
            'email' => 'warispc12334@gmail.com',
            'password' => bcrypt('password'),
        ]);

        Admin::insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
        ]);

        Theme::insert([
             'website_name' => "Tech Coderz",
             'website_tagline' => "Inspire the Next",
            'favicon' => ''
        ]);

        SiteSetting::insert([
            'email' => 'info@project.com'
        ]);

        Social::insert([
            'facebook' => ''
        ]);
    }
}
