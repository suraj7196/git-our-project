<?php

namespace App\Providers;

use App\Models\Theme;
use App\Models\SiteSetting;
use App\Models\Social;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        View::composer(['front.*'], function ($view){
           $view->with('theme', Theme::first());
        });

        View::composer(['front.*'], function ($view){
            $view->with('setting', SiteSetting::first());
        });

        View::composer(['front.*'], function ($view){
            $view->with('social', Social::first());
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
