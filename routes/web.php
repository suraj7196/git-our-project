<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Admin\AdminLoginController;
use App\Http\Controllers\Admin\AdminProfileController;
use App\Http\Controllers\Admin\ThemeController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\SocialController;
use App\Http\Controllers\Admin\PagesController;

use App\Http\Controllers\Front\IndexController;
use App\Http\Controllers\Front\FrontEndController;

//Index
Route::get('/', [IndexController::class, 'index'])->name('index');
// About Us
Route::get('/about-us', [FrontEndController::class, 'aboutUs'])->name('aboutUs');
// Testimonials
Route::get('/testimonial', [FrontEndController::class, 'testimonial'])->name('testimonial');
// Blog
Route::get('/blog', [FrontEndController::class, 'blog'])->name('blog');
// Blog Detail
Route::get('/blog/{slug}', [FrontEndController::class, 'blogDetail'])->name('blogDetail');
// Category Based Blogs
Route::get('/category/{slug}', [FrontEndController::class, 'categoryBlog'])->name('categoryBlog');
// Pricings
Route::get('/pricings', [FrontEndController::class, 'pricing'])->name('pricing');
Route::prefix('/admin')->group(function (){

    // Admin Login
    Route::get('/login', [AdminLoginController::class, 'adminLogin'])->name('adminLogin');
    Route::post('/login', [AdminLoginController::class, 'loginAdmin'])->name('loginAdmin');

    Route::group(['middleware' => 'admin'], function(){
    //Admin Dashboard
    Route::get('/dashboard', [AdminLoginController::class, 'adminDashboard'])->name('adminDashboard');

    // Admin Profile
        Route::get('/profile', [AdminProfileController::class, 'adminProfile'])->name('adminProfile');

        // Admin Profile Update
        Route::post('/profile/update/{id}', [AdminProfileController::class, 'adminProfileUpdate'])->name('adminProfileUpdate');

        // Delete Image
        Route::get('/delete-image/{id}', [AdminProfileController::class, 'deleteImage'])->name('deleteImage');

        // Change Password
        Route::get('/change-password', [AdminProfileController::class, 'changePassword'])->name('changePassword');

        // Check Current Password
        Route::post('/check-password', [AdminProfileController::class, 'chkUserPassword'])->name('chkUserPassword');

        // Theme Settings
        Route::get('/theme', [ThemeController::class, 'theme'])->name('theme');
        Route::post('/theme/{id}', [ThemeController::class, 'themeUpdate'])->name('themeUpdate');

        // Site Settings
        Route::get('/settings', [SettingController::class, 'settings'])->name('settings');
        Route::post('/settings/{id}', [SettingController::class, 'settingsUpdate'])->name('settingsUpdate');

        // Site Settings
        Route::get('/social', [SocialController::class, 'social'])->name('social');
        Route::post('/social/{id}', [SocialController::class, 'socialUpdate'])->name('socialUpdate');


        // About Us Page
        Route::get('/about', [PagesController::class, 'about'])->name('about');
        Route::post('/about/{id}', [PagesController::class, 'aboutUpdate'])->name('aboutUpdate');
    });

    Route::get('logout', [AdminLoginController::class, 'adminLogout'])->name('adminLogout');
});



