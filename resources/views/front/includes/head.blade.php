<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Author: HiBootstrap Theme, Category: IT, Multipurpose, HTML, SASS" />
    <!-- title -->
    <title>@yield('site_title')</title>
    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/bootstrap.min.css') }}" />
    <!-- fontawesome CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/fontawesome.min.css') }} " />
    <!-- envy icon CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/envyicon.min.css') }} " />
    <!-- animate CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/animate.min.css') }} " />
    <!-- magnific popup CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/magnific-popup.min.css') }} " />
    <!-- owl-carousel CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/owl.carousel.min.css') }} " />
    <!-- meanmenu CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/meanmenu.min.css') }} " />
    <!-- main style CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/style.css') }} " />
    <!-- responsive CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/responsive.css') }} " />
    <!-- favicon -->
    <link rel="icon" href="{{ asset('public/uploads/'.$theme->favicon) }}" type="image/png" sizes="16x16" />

    <style>
        .slider-item .inner-text span{
            color: white !important;
            font-family: roboto,sans-serif !important;
        }

        .pagination li {
            padding: 10px;
            border: 1px solid blue;
            width: 43px;
            margin-left: 10px;
        }
    </style>
</head>
